---
title: vyprvpn账户付款未成功账户被暂时锁定的解决方案
date: 2020-03-10 15:28:23
tags:
  - 账户被锁定
---
我的账户被锁定。我如何更新我的账单信息或重试支付？
 

如果在处理您的付款时您的帐户的文件的付款信息失败，您的帐户将被暂停。 在您再次使用帐户之前，您需要更新帐单信息或重试系统中已有的信息。

使用您的用户名和密码登录时，您可以进行这些更改 我们的网站 此处。

如果您忘记了密码，可以重置密码 此处。

请注意：如果您在更新付款时仍然遇到问题，请尝试清除浏览器的缓存和Cookie或尝试使用其他浏览器。

如果您用信用卡付款：

1.选择左侧的“帐户”选项卡。

![account_en.png](https://support.goldenfrog.com/hc/article_attachments/360008552152/account_en.png)
<!--more-->
2.从那里点击“结算信息”标题旁边的“编辑”链接。

![main_en.png](https://support.goldenfrog.com/hc/article_attachments/360008587471/main_en.png)

3. 输入请求的信息, 然后单击 "保存"。

![billinginfo_us.png](https://support.goldenfrog.com/hc/article_attachments/360008552392/billinginfo_us.png)

如果您通过PayPal付款：

1. 选择左侧的“帐户”选项卡。

![main_en.png](https://support.goldenfrog.com/hc/article_attachments/360008587471/main_en.png)

2.从那里点击“结算信息”标题旁边的“编辑”链接。

![account_en.png](https://support.goldenfrog.com/hc/article_attachments/360008552152/account_en.png)

3.输入与PayPal帐户相关联的电子邮件地址。 输入所需信息后，单击继续开票。

![paypal2_en.png](https://support.goldenfrog.com/hc/article_attachments/360008552172/paypal2_en.png)

4.单击与Paypal签出，以继续并进行结算协议。

![paypal_en.png](https://support.goldenfrog.com/hc/article_attachments/360008552192/paypal_en.png)

不管您的付款方式如何，请稍后5-10分钟付款更新。 一旦我们成功收到您的付款，您的帐户将会解锁并准备就绪。

如果您在PayPal结算协议中遇到问题，请确保以下内容：

您的PayPal帐户已验证。
您同时拥有付款来源和备用付款来源。
您的帐户设置为以美元汇款。
如果您的帐户满足所有这些要求，但您仍然无法汇款，则将VyprVPN计划的费用转入PayPal帐户，然后按照上述流程制定新的结算协议。
由于银行法律因国家/地区不同而不同的银行以不同的方式验证购买，您可能需要联系PayPal或您的银行，以确定在您尝试汇款付款时是否有任何问题阻止您付款给我们，并且继续 失败。

如果您需要任何进一步的帮助，请联系支持部门，我们将竭诚为您服务！