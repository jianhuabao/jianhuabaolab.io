---
title: vyprvpn账户支付宝，银联付款成功后账户被暂时锁定的解决方案
date: 2020-03-10 15:40:20
tags:
  - 账户被锁定
---
# vyprvpn账户使用支付宝，银联注册付款成功后账户但显示账户付款失败而被暂时锁定的解决方案
![](/images/SharedScreenshot1.jpg)
<!--more-->
![](/images/SharedScreenshot2.jpg)
显示付款时间的交易收据屏幕截图，工作人员会协助你很快解锁锁定账户，联系邮箱support@vyprvpn.com 或者support@goldenfrog.com
注册成功后请进入刷新一下的 [登录界面](https://b338fa80-1789-432a-80ff-4e859227d5cd.netrule.net/zh/login)
## [30天无条件退款](http://www.getvy.net/zh/refer/china?offer_id=235&aff_id=4321)